<?php

namespace Drupal\social_migration\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_plus\Entity\Migration;

/**
 * Class GenericDeleteForm.
 */
class GenericDeleteForm extends ConfirmFormBase {

  /**
   * The migration to confirm.
   *
   * @var \Drupal\migrate_plus\Entity\Migration
   */
  protected $migration;

  /**
   * The URL to return to after the delete operation is complete/canceled.
   *
   * @var \Drupal\Core\Url
   */
  protected $returnUrl;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %migration migration?', [
      '%migration' => $this->migration->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->returnUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'soc_mig_admin_generic_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Migration $migration = NULL) {
    $this->migration = $migration;
    $group = $migration->migration_group;
    if (preg_match('/^social_migration_(.*)_feeds_group$/', $group, $matches) === 1) {
      $this->returnUrl = Url::fromRoute('social_migration.' . $matches[1] . '.list');
    }
    else {
      $this->returnUrl = Url::fromRoute('social_migration.main');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $migrationId = $this->migration->id();
    $this->migration->delete();
    $form_state->setRedirectUrl($this->returnUrl);
    $this->messenger()->addStatus($this->t('Successfully removed the %id migration.', [
      '%id' => $migrationId,
    ]));
  }

}
